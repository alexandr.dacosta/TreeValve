/*
File        : gpio.c
Version     : 1.0
Author(s)   : Bruelhart Harold
Date        : 19.08.15
Description : Methods for use the gpio
Projet		: Building Gateway
Company 	: HEAI-FR
*/

/* Linux include */
	#include <stdio.h>
	#include <stdlib.h>
	#include <stdint.h>
	#include <unistd.h>
	#include <string.h>
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <fcntl.h>
	#include <errno.h>
	#include <syslog.h>
/* Project include */

	/* Constant for gpio */
	#include "gpio_cst.h"

/* Methods */

	/* Export gpio */
	int gpio_export(char * gpio_name) {

		/* Flag for errors */
		int ret = 0;

		/* File descriptor */
		int gpio_export_fd;

		/* Open file descriptor */
		gpio_export_fd = open(GPIO_PATH_1, O_WRONLY);

		/* Control */
		if (gpio_export_fd < 0) ret += -1;

		/* Convert and write */
		write(gpio_export_fd, gpio_name, strlen(gpio_name));

		/* Close file descriptor */
		close(gpio_export_fd);

		/* Return */
		return ret;
	}

	/* Init GPIO */
	int gpio_init(char * gpio_name, uint8_t type) {

		/* Flag for errors */
		int ret = 0;

		/* File descriptor */
		int gpio_init_fd;

		/* Path to gpio */
		char * gpio_path = malloc(sizeof(char) * GPIO_PATH_SIZE);
		if (gpio_path == NULL) return -2;
		memset(gpio_path, '\0', GPIO_PATH_SIZE);

		/* Define path */
		memcpy(gpio_path, GPIO_PATH_2, GPIO_PATH_2_SIZE);
		strcat(gpio_path, GPIO);
		strcat(gpio_path, gpio_name);
		strcat(gpio_path, GPIO_DIRECTION);

		/* Open file descriptor */
		gpio_init_fd = open(gpio_path, O_RDWR);
		if (gpio_init_fd < 0){
			ret += -1;
			fprintf(stderr,"Cannot open file errno:%d",errno);

		}

		/* Define type and write */
		if (type == GPIO_OUT) write(gpio_init_fd, GPIO_OUT_STR, GPIO_OUT_SIZE);
		else write(gpio_init_fd, GPIO_IN_STR, GPIO_IN_SIZE);

		/* Close file descriptor */
		close(gpio_init_fd);

		/* Release */
		free(gpio_path);

		/* Return */
		return ret;
	}

	/* Write gpio */
	int gpio_write(char * gpio_name, uint8_t gpio_value) {

		/* Flag for errors */
		int ret = 0;

		/* File descriptor */
		int gpio_write_fd;

		/* Path to gpio */
		char * gpio_path = malloc(sizeof(char) * GPIO_PATH_SIZE);
		if (gpio_path == NULL) return -2;
		memset(gpio_path, '\0', GPIO_PATH_SIZE);

		/* Define path */
		memcpy(gpio_path, GPIO_PATH_2, GPIO_PATH_2_SIZE);
		strcat(gpio_path, GPIO);
		strcat(gpio_path, gpio_name);
		strcat(gpio_path, GPIO_VALUE);
		
		/* Open file descriptor */
		gpio_write_fd = open(gpio_path, O_RDWR);
		if (gpio_write_fd < 0) ret += -1;

		/* Write */
		if (gpio_value == GPIO_HIGH) write(gpio_write_fd, GPIO_HIGH_STR, GPIO_VALUE_SIZE);
		if (gpio_value == GPIO_LOW) write(gpio_write_fd, GPIO_LOW_STR, GPIO_VALUE_SIZE);
		
		/* Close file descriptor */
		close(gpio_write_fd);

		/* Release */
		free(gpio_path);

		/* Return */
		return ret;
	}

	/* Read gpio */
	uint8_t gpio_read(char * gpio_name, int * error_flag) {

		/* Flag for errors */
		int ret = 0;

		/* File descriptor */
		int gpio_read_fd;

		/* Values */
		char * gpio_value_char = malloc(sizeof(char) * GPIO_VALUE_CHAR_SIZE);
		if (gpio_value_char == NULL) return -2;
		memset(gpio_value_char, '\0', GPIO_VALUE_CHAR_SIZE);
		uint8_t gpio_value_uint = 2;

		/* Path to gpio */
		char * gpio_path = malloc(sizeof(char) * GPIO_PATH_SIZE);
		if (gpio_path == NULL) return -2;
		memset(gpio_path, '\0', GPIO_PATH_SIZE);

		/* Define path */
		memcpy(gpio_path, GPIO_PATH_2, GPIO_PATH_2_SIZE);
		strcat(gpio_path, GPIO);
		strcat(gpio_path, gpio_name);
		strcat(gpio_path, GPIO_VALUE);
		
		/* Open file descriptor */
		gpio_read_fd = open(gpio_path, O_RDONLY);
		if (gpio_read_fd < 0) ret += -1;

		/* Read */
		read(gpio_read_fd, &gpio_value_char[0], GPIO_VALUE_CHAR_SIZE);

		/* Return right value */
		if (gpio_value_char[0] == '0') gpio_value_uint = 0;
		if (gpio_value_char[0] == '1') gpio_value_uint = 1;

		/* Close file descriptor */
		close(gpio_read_fd);

		/* Release */
		free(gpio_path);
		free(gpio_value_char);

		/* Errors */
		(*error_flag) = ret;

		/* Return */
		return gpio_value_uint;
	}

	/* Unexport gpio */
	int unexport_gpio(char * gpio_name) {

		/* Flag for errors */
		int ret = 0;

		/* File descriptor */
		int gpio_unexport_fd;

		/* Open file descriptor */
		gpio_unexport_fd = open(GPIO_PATH_3, O_WRONLY);

		/* Control */
		if (gpio_unexport_fd < 0) ret += -1;

		/* Convert and write */
		write(gpio_unexport_fd, gpio_name, strlen(gpio_name));

		/* Close file descriptor */
		close(gpio_unexport_fd);

		/* Return */
		return ret;
		
	}

	/* Get gpio file descriptor */
	int gpio_get_file_descriptor(char * gpio_name, int * pt_error) {

		/* File descriptor */
		int gpio_get_fd;

		/* Path to gpio */
		char * gpio_path = malloc(sizeof(char) * GPIO_PATH_SIZE);
		if (gpio_path == NULL) return -2;
		memset(gpio_path, '\0', GPIO_PATH_SIZE);

		/* Define path */
		memcpy(gpio_path, GPIO_PATH_2, GPIO_PATH_2_SIZE);
		strcat(gpio_path, GPIO);
		strcat(gpio_path, gpio_name);
		strcat(gpio_path, GPIO_VALUE);
		
		/* Open file descriptor */
		gpio_get_fd = open(gpio_path, O_RDONLY);
		if (gpio_get_fd < 0) (*pt_error) += -1;

		/* Release */
		free(gpio_path);

		/* Return file desciptor */
		return gpio_get_fd;
	}

	/* read_gpio_by_file_descriptor */
	int gpio_read_by_file_decriptor(int file_descriptor, int * pt_error) {

		/* Variables */
		int ret = 0;
		uint8_t gpio_value_uint = 2;

		/* Values */
		char * gpio_value_char = malloc(sizeof(char) * GPIO_VALUE_CHAR_SIZE);
		if (gpio_value_char == NULL) return -2;
		memset(gpio_value_char, '\0', GPIO_VALUE_CHAR_SIZE);

		/* Read */
		read(file_descriptor, &gpio_value_char[0], GPIO_VALUE_CHAR_SIZE);

		/* Return right value */
		if (gpio_value_char[0] == '0') gpio_value_uint = 0;
		if (gpio_value_char[0] == '1') gpio_value_uint = 1;

		/* Release */
		free(gpio_value_char);

		/* Check value */
		if (gpio_value_uint == 2) {

			/* Set error */
			ret += - 1;
		}

		/* Errors */
		(*pt_error) = ret;

		/* Return */
		return gpio_value_uint;
	}
