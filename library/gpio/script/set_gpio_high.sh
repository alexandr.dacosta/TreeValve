#!/bin/bash

# Set to high state
echo "1" > /sys/class/gpio/gpio113/value
echo "1" > /sys/class/gpio/gpio124/value
echo "1" > /sys/class/gpio/gpio126/value
echo "1" > /sys/class/gpio/gpio133/value
echo "1" > /sys/class/gpio/gpio134/value
echo "1" > /sys/class/gpio/gpio112/value
echo "1" > /sys/class/gpio/gpio118/value
echo "1" > /sys/class/gpio/gpio122/value
echo "1" > /sys/class/gpio/gpio127/value
echo "1" > /sys/class/gpio/gpio117/value
echo "1" > /sys/class/gpio/gpio121/value
echo "1" > /sys/class/gpio/gpio123/value
echo "1" > /sys/class/gpio/gpio115/value
echo "1" > /sys/class/gpio/gpio140/value
echo "1" > /sys/class/gpio/gpio141/value

exit 0
