#!/bin/bash

# Export GPIO line
echo "113" > /sys/class/gpio/export # GPIO4_17
echo "124" > /sys/class/gpio/export # GPIO4_28
echo "126" > /sys/class/gpio/export # GPIO4_30
echo "133" > /sys/class/gpio/export # GPIO5_05
echo "134" > /sys/class/gpio/export # GPIO5_06
echo "135" > /sys/class/gpio/export # GPIO5_07
echo "112" > /sys/class/gpio/export # GPIO4_16
echo "120" > /sys/class/gpio/export # GPIO4_24
echo "119" > /sys/class/gpio/export # GPIO4_23
echo "125" > /sys/class/gpio/export # GPIO4_29
echo "137" > /sys/class/gpio/export # GPIO5_09
echo "136" > /sys/class/gpio/export # GPIO5_08
echo "114" > /sys/class/gpio/export # GPIO4_18
echo "118" > /sys/class/gpio/export # GPIO4_22
echo "122" > /sys/class/gpio/export # GPIO4_26
echo "127" > /sys/class/gpio/export # GPIO4_31
echo "117" > /sys/class/gpio/export # GPIO4_21
echo "121" > /sys/class/gpio/export # GPIO4_25
echo "123" > /sys/class/gpio/export # GPIO4_27
echo "115" > /sys/class/gpio/export # GPIO4_19
echo "140" > /sys/class/gpio/export # GPIO5_12
echo "141" > /sys/class/gpio/export # GPIO5_13
echo "7" > /sys/class/gpio/export   # GPIO1_07

# Set direction
echo "out" > /sys/class/gpio/gpio113/direction
echo "out" > /sys/class/gpio/gpio124/direction
echo "out" > /sys/class/gpio/gpio126/direction
echo "out" > /sys/class/gpio/gpio133/direction
echo "out" > /sys/class/gpio/gpio134/direction
echo "in" > /sys/class/gpio/gpio135/direction
echo "out" > /sys/class/gpio/gpio112/direction
echo "in" > /sys/class/gpio/gpio120/direction
echo "in" > /sys/class/gpio/gpio119/direction
echo "in" > /sys/class/gpio/gpio125/direction
echo "in" > /sys/class/gpio/gpio137/direction
echo "in" > /sys/class/gpio/gpio136/direction
echo "in" > /sys/class/gpio/gpio114/direction
echo "out" > /sys/class/gpio/gpio118/direction
echo "out" > /sys/class/gpio/gpio122/direction
echo "out" > /sys/class/gpio/gpio127/direction
echo "out" > /sys/class/gpio/gpio117/direction
echo "out" > /sys/class/gpio/gpio121/direction
echo "out" > /sys/class/gpio/gpio123/direction
echo "out" > /sys/class/gpio/gpio115/direction
echo "out" > /sys/class/gpio/gpio140/direction
echo "out" > /sys/class/gpio/gpio141/direction
echo "out" > /sys/class/gpio/gpio7/direction

exit 0

