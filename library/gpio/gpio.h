/*
File        : gpio.h
Version     : 1.0
Author(s)   : Bruelhart Harold
Date        : 19.08.15
Description : Headers for use the gpio
Projet		: Building Gateway
Company 	: HEAI-FR
*/

/* Linux include */
	#include <stdint.h>

#pragma once
#ifndef gpio_h
#define gpio_h

/* Headers */

	/**
	 * Name		   : gpio_export
	 * Description : Sequence for export a gpio
	 *
	 * @param char * gpio_name	(Name of gpio)
	 * @return int				(Flag for errors)
	*/
	extern int gpio_export(char * gpio_name);

	/**
	 * Name		   : gpio_init
	 * Description : Sequence for init a gpio (direction)
	 *
	 * @param char * gpio_name	(Name of gpio)
	 * @param uint8_t type		(Type, in or out)
	 * @return int				(Flag for errors)
	*/
	extern int gpio_init(char * gpio_name, uint8_t type);

	/**
	 * Name		   : gpio_write
	 * Description : Sequence for write a gpio
	 *
	 * @param char * gpio_name		(Name of gpio)
	 * @param uint8_t gpio_value	(Value to write)
	 * @return int					(Flag for errors)
	*/
	extern int gpio_write(char * gpio_name, uint8_t gpio_value);

	/**
	 * Name		   : gpio_read
	 * Description : Sequence for read a gpio
	 *
	 * @param char * gpio_name	(Name of gpio)
	 * @param int * error_flag	(Flag for errors)
	 * @return uint8_t			(Read value)
	*/
	extern uint8_t gpio_read(char * gpio_name, int * error_flag);

	/**
	 * Name		   : gpio_unexport
	 * Description : Sequence for unexport a gpio
	 *
	 * @param char * gpio_name	(Name of gpio)
	 * @return int				(Flag for errors)
	*/
	extern int unexport_gpio(char * gpio_name);

	/**
	 * Name		   : gpio_get_file_descriptor
	 * Description : Sequence for get a gpio file descriptor
	 *
	 * @param char * gpio_name	(Name of gpio)
	 * @param int * error_flag	(Flag for errors)
	 * @return int				(File descriptor)
	*/
	extern int gpio_get_file_descriptor(char * gpio_name, int * pt_error);

	/**
	 * Name		   : gpio_read_by_file_decriptor
	 * Description : Sequence for read a gpio with his file descriptor
	 *
	 * @param int file_descriptor	(Name of gpio)
	 * @param int * pt_error		(Flag for errors)
	 * @return uint8_t				(Read value)
	*/
	extern int gpio_read_by_file_decriptor(int file_descriptor, int * pt_error);

#endif

