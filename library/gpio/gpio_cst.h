/*
File        : gpio_cst.h
Version     : 1.0
Author(s)   : Bruelhart Harold
Date        : 19.08.15
Description : Constants for use the gpio
Projet		: Building Gateway
Company 	: HEAI-FR
*/

#pragma once
#ifndef gpio_cst_h
#define gpio_cst_h

	/* Data size */
	#define GPIO_PATH_SIZE			100
	#define GPIO_PATH_2_SIZE		16
	#define GPIO_OUT_SIZE			3
	#define GPIO_IN_SIZE			2
	#define GPIO_VALUE_SIZE			1
	#define GPIO_VALUE_CHAR_SIZE	2

	/* Path definition */
	#define GPIO_PATH_2				"/sys/class/gpio/"
	#define GPIO_PATH_1				"/sys/class/gpio/export"
	#define GPIO_PATH_3				"/sys/class/gpio/unexport"
	#define GPIO_DIRECTION			"/direction"
	#define GPIO_VALUE				"/value"
	#define GPIO 					"gpio"

	/* GPIO state */
	#define GPIO_OUT_STR			"out"
	#define GPIO_IN_STR				"in"
	#define GPIO_OUT				0
	#define GPIO_IN					1

	/* GPIO value */
	#define GPIO_LOW_STR			"0"
	#define GPIO_HIGH_STR			"1"
	#define GPIO_LOW				0
	#define GPIO_HIGH				1

	/* GPIO name */
	#define VALVE_1					"17"
	#define VALVE_2					"22"
	#define VALVE_3					"27"

#endif
	
