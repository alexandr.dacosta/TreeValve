#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "../io_expander/io_expander.h"
#include "../gpio/gpio.h"
#include "../spi/spi.h"

#include "../gpio/gpio_cst.h"

/* Constants */
	 #define DIG_IN_2		"87"

int main(int argc, char *argv[])
{
	/* Main variables */
	int ret = 0;
	int * pt_ret = &ret;
	uint8_t read_value = 0;

	/* 1 : GPIO test */

		/* Write sequence */
			/* Initial tree */
			printf("Initial GPIO :\n");
			system("ls /sys/class/gpio/");
			printf("\n");
			sleep(1);

			/* GPIO export */
			if (gpio_export(DIG_IN_2) < 0) printf("Error in gpio_export\n");

			/* GPIO init */
			if (gpio_init(DIG_IN_2, GPIO_OUT) < 0) printf("Error in gpio_init\n");

			/* GPIO write high*/
			if (gpio_write(DIG_IN_2, GPIO_HIGH) < 0) printf("Error in gpio_write\n");
			printf("GPIO in high state\n");
			sleep(10);

			/* GPIO write low*/
			if (gpio_write(DIG_IN_2, GPIO_LOW) < 0) printf("Error in gpio_write\n");
			printf("GPIO in low state\n");
			sleep(10);

			/* GPIO unexport */
			if (unexport_gpio(DIG_IN_2) < 0) printf("Error in gpio_unexport\n");

			/* Final tree */
			sleep(1);
			printf("Final GPIO :\n");
			system("ls /sys/class/gpio/");
			printf("\n");

		/* Read sequence */
			/* Initial tree */
			printf("Initial GPIO :\n");
			system("ls /sys/class/gpio/");
			printf("\n");

			/* GPIO export */
			if (gpio_export(DIG_IN_2) < 0) printf("Error in gpio_export\n");

			/* GPIO init */
			if (gpio_init(DIG_IN_2, GPIO_IN) < 0) printf("Error in gpio_init\n");

			/* GPIO read high*/
			while (!read_value) {
				read_value = gpio_read(DIG_IN_2, pt_ret);
				printf("value = %d\n", read_value);
				sleep(1);
			}

			/* GPIO unexport */
			if (unexport_gpio(DIG_IN_2) < 0) printf("Error in gpio_unexport\n");

			/* Final tree */
			sleep(1);
			printf("Final GPIO :\n");
			system("ls /sys/class/gpio/");
			printf("\n");

}
