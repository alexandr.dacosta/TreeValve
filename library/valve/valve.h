/*
File        : valve.h
Version     : 1.0
Author(s)   : Bruelhart Harold
Date        : 18.12.17
Description : Headers for use the valve
Projet		: LIER
Company 	: HEAI-FR
*/

/* Linux include */
	#include <stdint.h>

#pragma once
#ifndef valve_h
#define valve_h

/* Headers */

	/**
	 * Name		   : init_valve_gpio
	 * Description : Init gpio for use valve
	 *
	 * @param void
	 * @return int				(Flag for errors)
	*/
	int init_valve_gpio(void);

	/**
	 * Name		   : open_ecs
	 * Description : Open ecs for LIER
	 *
	 * @param void
	 * @return int				(Flag for errors)
	*/
	int open_ecs(void);

        /**
         * Name            : open_first_valve
         * Description : Open first vavle for LIER
         *
         * @param void
         * @return int                          (Flag for errors)
        */
        int open_first_valve(void);

        /**
         * Name            : close_first_valve
         * Description : Close first valve for LIER
         *
         * @param void
         * @return int                          (Flag for errors)
        */
        int close_first_valve(void);


	/**
	 * Name		   : close_ecs
	 * Description : Close ecs for LIER
	 *
	 * @param void
	 * @return int				(Flag for errors)
	*/
	int close_ecs(void);

	/**
	 * Name		   : get_valve_state
	 * Description : State of valves
	 *
	 * @param void
	 * @return int				(Flag for errors and valve state)
	*/
	int get_valve_state(void);

	/**
	 * Name		   : get_temperature
	 * Description : Get external temperature
	 *
	 * @param float * pt_value 	(Return value)
	 * @return int				(Flag for errors)
	*/
	int get_temperature(float * pt_value);

#endif
