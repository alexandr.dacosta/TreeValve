/*
File        : valve.c
Version     : 1.0
Author(s)   : Bruelhart Harold
Date        : 15.12.17
Description : Methods for use the valve
Projet		: LIER
Company 	: HEAI-FR
*/

/* Linux include */
	#include <stdio.h>
	#include <stdlib.h>
	#include <stdbool.h>
	#include <pthread.h>
	#include <stdint.h>
	#include <unistd.h>
	#include <string.h>
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <fcntl.h>
	#include <errno.h>
	#include <syslog.h>

/* Project include */
	#include "/usr/include/curl/curl.h"
	#include "../cJSON/cJSONFiles/cJSON/cJSON.h"
	#include "../gpio/gpio.h"
	#include "../gpio/gpio_cst.h"
	#include "../valve/valve_cst.h"

/* url structure */
struct url_data {
	size_t size;
	char* data;
};

/* Data return */
size_t write_data(void *ptr, size_t size, size_t nmemb, struct url_data *data) {
	size_t index = data->size;
	size_t n = (size * nmemb);
	char* tmp;

	data->size += (size * nmemb);

#ifdef DEBUG
	fprintf(stderr, "data at %p size=%ld nmemb=%ld\n", ptr, size, nmemb);
#endif
	tmp = (char*)realloc(data->data, data->size + 1); /* +1 for '\0' */

	if(tmp) {
	    data->data = tmp;
	} else {
	    if(data->data) {
	        free(data->data);
	    }
	    fprintf(stderr, "Failed to allocate memory.\n");
	    return 0;
	}

	memcpy((data->data + index), ptr, n);
	data->data[data->size] = '\0';

	return size * nmemb;
}

int init_valve_gpio(void) {

	/* Export gpio */
	if (gpio_export(VALVE_1) < 0) return EXPORT_V1_ER;
	if (gpio_export(VALVE_2) < 0) return EXPORT_V2_ER;
	if (gpio_export(VALVE_3) < 0) return EXPORT_V3_ER;

	/* Set gpio function */
	if (gpio_init(VALVE_1, GPIO_OUT) < 0) return FUNCTION_V1_ER;
	if (gpio_init(VALVE_2, GPIO_OUT) < 0) return FUNCTION_V2_ER;
	if (gpio_init(VALVE_3, GPIO_OUT) < 0) return FUNCTION_V3_ER;

	/* Return */
	return 0;
}

int open_first_valve(void) {

        /* Open valve 1 */
        if (gpio_write(VALVE_1, GPIO_HIGH) < 0) return OPEN_V1_ER;

        /* Wait a few moment */
        sleep(MOVE_V1_TM);

        /* Log informations */
        syslog(LOG_NOTICE,"%lu [ECS controller] First valve open by system\n", pthread_self());
	
	/* Return */
	return 0;
}

int close_first_valve(void) {

        /* Close valve 1 */
        if (gpio_write(VALVE_1, GPIO_LOW) < 0)  return OPEN_V1_ER;

        /* Wait a few moment */
        sleep(MOVE_V1_TM);

        /* Log informations */
        syslog(LOG_NOTICE,"%lu [ECS controller] First valve close by system\n", pthread_self());

        /* Return */
        return 0;
}

int open_ecs(void) {

	/* Open valve 1 */
	if (gpio_write(VALVE_1, GPIO_HIGH) < 0)	return OPEN_V1_ER;

	/* Wait a few moment */
	sleep(MOVE_V1_TM);

	/* Open valve 2 */
	if (gpio_write(VALVE_2, GPIO_HIGH) < 0)	return OPEN_V2_ER;

	/* Wait a few moment */
	sleep(MOVE_V2_TM);

	/* Open valve 3 */
	if (gpio_write(VALVE_3, GPIO_HIGH) < 0)	return OPEN_V3_ER;

	/* Wait a few moment */
	sleep(MOVE_V3_TM);

	/* Log informations */
	syslog(LOG_NOTICE,"%lu [ECS controller] ECS open by system\n", pthread_self());

	/* Return */
	return 0;
}

int close_ecs(void) {

	/* Close valve 3 */
	if (gpio_write(VALVE_3, GPIO_LOW) < 0)	return OPEN_V3_ER;

	/* Wait a few moment */
	sleep(MOVE_V3_TM);

	/* Close valve 2 */
	if (gpio_write(VALVE_2, GPIO_LOW) < 0)	return OPEN_V2_ER;

	/* Wait a few moment */
	sleep(MOVE_V2_TM);

	/* Close valve 1 */
	if (gpio_write(VALVE_1, GPIO_LOW) < 0)	return OPEN_V1_ER;

	/* Wait a few moment */
	sleep(MOVE_V1_TM);

	/* Log informations */
	syslog(LOG_NOTICE,"%lu [ECS controller] ECS close by system\n", pthread_self());

	/* Return */
	return 0;
}

int get_valve_state(void) {

	/* Variables */
	int ret = 0;
	int * pt_er = &ret;
	uint8_t read_value = 0;
	uint8_t final_value = 0;

	/* Get state of valve 1 */
	read_value = gpio_read(VALVE_1, pt_er);

	/* Check error */
	if ((*pt_er) < 0) return READ_V1_ER;

	/* Add value */
	final_value += read_value;

	/* Get state of valve 1 */
	read_value = gpio_read(VALVE_2, pt_er);

	/* Check error */
	if ((*pt_er) < 0) return READ_V2_ER;

	/* Add value */
	read_value = read_value << 1;
	final_value += read_value;

	/* Get state of valve 1 */
	read_value = gpio_read(VALVE_3, pt_er);

	/* Check error */
	if ((*pt_er) < 0) return READ_V3_ER;

	/* Add value */
	read_value = read_value << 2;
	final_value += read_value;

	/* Return */
	return final_value;
}

int get_temperature(float * pt_value) {

	/* Variables */
	int error = 0;

	/* Libcurl variables */
	CURL *curl;
	CURLcode res;
	struct curl_slist *chunk = NULL;

	/* Libcurl init */
	curl = curl_easy_init();

	/* libcurl headers */
	if(curl) {
	
		/* Set chunk */
		chunk = curl_slist_append(chunk, "Content-Type: application/json; charset=utf-8"); 
		res = curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
	}

	/* Return data structure */
	struct url_data data; data.size = 0;

	/* Allocate data */
	data.data = (char*)malloc(RET_DATA_SIZE); 
	if(NULL == data.data) {
		fprintf(stderr, "Failed to allocate memory.\n");
		return -2;
	}

	/* Send request and get response */
	res = curl_easy_setopt(curl, CURLOPT_URL, BBDATA_URL); 
	res = curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);
	res = curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, false);
	res = curl_easy_setopt(curl, CURLOPT_TIMEOUT, COM_TIMEOUT);
	res = curl_easy_setopt(curl, CURLOPT_USERPWD, AUTHENTIFICATION);
	res = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
 	res = curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);
	res = curl_easy_perform(curl);

	/* Get data */
	(*pt_value) = atof(data.data);

	/* Log response */
	syslog(LOG_NOTICE,"%lu [ECS controller] Outdoor temperature is %s\n", pthread_self(), data.data);

	/* libcurl errors */
	if(res != CURLE_OK) {

		/* Log response */
		syslog(LOG_NOTICE,"%lu [ECS controller] Unable to get outdoor temperature %s\n", pthread_self(), curl_easy_strerror(res));

		/* Return error */
		error = -1;
	}

	/* Free data */
	free(data.data);

	/* free the custom headers */
	curl_slist_free_all(chunk);

 	/* always cleanup */
	curl_easy_cleanup(curl);

	/* Global cleanup */
	curl_global_cleanup();

	/* Return value */
	return error;
}
