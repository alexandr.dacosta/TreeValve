/*
File        : valve_cst.h
Version     : 1.0
Author(s)   : Bruelhart Harold
Date        : 18.12.17
Description : Constants for use the valve
Projet		: LIER
Company 	: HEAI-FR
*/

#pragma once
#ifndef	valve_cst_h
#define valve_cst_h

	/* Error */
	#define EXPORT_V1_ER	-10
	#define EXPORT_V2_ER	-11
	#define EXPORT_V3_ER	-12
	#define FUNCTION_V1_ER	-20
	#define FUNCTION_V2_ER	-21
	#define FUNCTION_V3_ER	-22
	#define OPEN_V1_ER		-30
	#define OPEN_V2_ER		-31
	#define OPEN_V3_ER		-32
	#define READ_V1_ER		-40
	#define READ_V2_ER		-41
	#define READ_V3_ER		-42

	/* Time */
	#define MOVE_V1_TM		35
	#define MOVE_V2_TM		35
	#define MOVE_V3_TM		35

	/* BBDATA constants */
	#define BBDATA_URL			"https://192.168.0.1:8443/LIERApp/lier/rest/objects/146/"
	#define COM_TIMEOUT			5
	#define AUTHENTIFICATION	"lier-user:fallu68$japp"
	
	/* Data size */
	#define RET_DATA_SIZE		1024
	

#endif
