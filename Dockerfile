# Base image
FROM balenalib/raspberry-pi:build

RUN mkdir -p /usr/include/curl
COPY dependencies/curl/ /usr/include/curl
RUN mkdir /TreeValve
COPY config/ /TreeValve/config
COPY main/ /TreeValve/main
COPY library/ /TreeValve/library
RUN mkdir /TreeValve/exec && cd /TreeValve/config && make