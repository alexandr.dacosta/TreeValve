/**
	Name : main.c
	Author : Brülhart Harold
	Description : network scan
**/

/* Linux include */
	#include <unistd.h>
	#include <stdlib.h>
	#include <pthread.h>
	#include <stdio.h>
	#include <stdbool.h>
	#include <unistd.h>
	#include <string.h>
	#include <time.h>
	#include <syslog.h>

/* Poject include */
	#include "main.h"
	#include "/usr/include/curl/curl.h"
	#include "../library/cJSON/cJSONFiles/cJSON/cJSON.h"
	#include "../library/valve/valve.h"
	#include "../library/valve/valve_cst.h"

int main(int argc, char *argv[]) {

	/* Variables */
	int flag_er = 0;
	float external_tmp = 0;
	float * external_tmp_pt = &external_tmp;

	/* Init system */
	flag_er = init_valve_gpio();

        /* Log informations */
        syslog(LOG_NOTICE,"%lu [ECS controller] ECS init result = %d\n", pthread_self(), flag_er);

	/* Close ecs */
	flag_er = close_ecs();

        /* Log informations */
        syslog(LOG_NOTICE,"%lu [ECS controller] ECS close result = %d\n", pthread_self(), flag_er);

	/* Return */
	return 0;
}
