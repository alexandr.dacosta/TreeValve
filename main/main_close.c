/**
	Name : main_open.c
	Author : Matthieu Jourdan
	Description : close ECS
**/

/* Linux include */
	#include <unistd.h>
	#include <stdlib.h>
	#include <pthread.h>
	#include <stdio.h>
	#include <stdbool.h>
	#include <unistd.h>
	#include <string.h>
	#include <time.h>
	#include <syslog.h>

/* Poject include */
	#include "main.h"
	#include "/usr/include/curl/curl.h"
	#include "../library/cJSON/cJSONFiles/cJSON/cJSON.h"
	#include "../library/valve/valve.h"
	#include "../library/valve/valve_cst.h"
	#include "../library/gpio/gpio_cst.h"
	#include "../library/gpio/gpio.h"

int main(int argc, char *argv[]) {

	/* Variables */
	int flag_er = 0;

	/* Init system */
    // init_valve_gpio() disabled as it would turn off all relays at init
    // and thus not respect the 3-2-1 turn off order
	//flag_er = init_valve_gpio();

	/* Export gpio */
	if (gpio_export(VALVE_1) < 0) return EXPORT_V1_ER;
	if (gpio_export(VALVE_2) < 0) return EXPORT_V2_ER;
	if (gpio_export(VALVE_3) < 0) return EXPORT_V3_ER;

    /* Log informations */
    syslog(LOG_NOTICE,"%lu [ECS controller] ECS init result = %d\n", pthread_self(), flag_er);

	/* Close ecs */
	flag_er = close_ecs();

    /* Log informations */
    syslog(LOG_NOTICE,"%lu [ECS controller] ECS close result = %d\n", pthread_self(), flag_er);

	/* Return */
	return 0;
}
