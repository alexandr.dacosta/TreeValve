/**
	Name : main.c
	Author : Brülhart Harold
	Description : network scan
**/

/* Linux include */
	#include <unistd.h>
	#include <stdlib.h>
	#include <pthread.h>
	#include <stdio.h>
	#include <stdbool.h>
	#include <unistd.h>
	#include <string.h>
	#include <time.h>
	#include <syslog.h>

/* Poject include */
	#include "main.h"
	#include "/usr/include/curl/curl.h"
	#include "../library/cJSON/cJSONFiles/cJSON/cJSON.h"
	#include "../library/valve/valve.h"
	#include "../library/valve/valve_cst.h"

int main(int argc, char *argv[]) {

	/* Variables */
	int flag_er = 0;
	float external_tmp = 0;
	float * external_tmp_pt = &external_tmp;

	/* Init system */
	flag_er = init_valve_gpio();

        /* Log informations */
        syslog(LOG_NOTICE,"%lu [ECS controller] ECS init result = %d\n", pthread_self(), flag_er);

	/* Wait a few moment */
	sleep(ECS_INIT_TIME);

	/* Infinite loop */
	while (true) {

		/* Get external temperature */
		flag_er = get_temperature(external_tmp_pt);

		/* Check if valve must be opened */
		if ((flag_er < 0) || (*external_tmp_pt < 0)) {

			/* Open ecs */
			flag_er = open_first_valve();

			/* Log informations */
			syslog(LOG_NOTICE,"%lu [ECS controller] First valve open result = %d\n", pthread_self(), flag_er);

			/* Wait a few moment */
			sleep(ECS_OPENED_TIME);

			/* Close ecs */
			flag_er = close_first_valve();

                        /* Log informations */
                        syslog(LOG_NOTICE,"%lu [ECS controller] First valve close result = %d\n", pthread_self(), flag_er);
		}

		else {

			/* TODO mutex condition */
			/* Must be check if valves are used by algorithm if no close for security */

			/* Check if ecs is open */
			if (get_valve_state() != ECS_IS_CLOSE) {
			
				/* Close */
				flag_er = close_first_valve();

                        	/* Log informations */
                        	syslog(LOG_NOTICE,"%lu [ECS controller] First vavle close result = %d\n", pthread_self(), flag_er);
			}
		}

		/* Wait a few moment */
		sleep(ECS_SLEEP_TIME);	
	}

	/* Return */
	return 0;
}
