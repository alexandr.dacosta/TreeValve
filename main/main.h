/*
File        : main.h
Version     : 1.0
Author(s)   : Bruelhart Harold
Date        : 09.08.17
Description : Constants for Main.cpp
Projet		: Smart Living Lab
Company 	: HEAI-FR
*/

/* File inclusion */
#ifndef MAIN
#define MAIN

	/* Time */
	#define ECS_OPENED_TIME		60
	#define ECS_SLEEP_TIME		600
	#define ECS_INIT_TIME		60

	/* State */ 
	#define ECS_IS_CLOSE		0
	#define ECS_IS_OPEN			7
	
#endif
